How To Git Flow
===============

In this tutorial, we're going to walk through a typical git-flow development
cycle. For further reading, `nvie's blog post`_ on the topic is the defacto
reference.

Clone the Repository
--------------------

First, we're going to clone the repository and configure it for git-flow.
Including the installation of the git-flow plugin. I've created a project for
this purpose.

Installation & Initialization
-----------------------------

Installation can be done in several ways. The easiest on osx is by using
homebrew. Linux and windows installation is left as an exercise for the reader,
but information can be found on the repos homepage_.


.. code-block:: shell

    % brew install git-flow

Next, we clone the repository

.. code-block:: shell

    % git clone https://bitbucket.org/jkyle/git-flow-tutorial.git
    % cd git-flow-tutorial
    % git branch master origin/master

Finally, we set up the git-flow plugin. The defaults are fine

.. code-block:: shell

    % git flow init -d

Git flow sets up several branch prefixes. The "flow" of commits between these
branches are what defines git-flow. The nvie reference covers this in detail,
but the git-flow plugin itself is verbose about what it's doing.

The Flow
--------

Feature Branches
^^^^^^^^^^^^^^^^

Most developers interact with a repository by creating feature (or issue)
branches and submitting pull request. That's the work flow we'll walk through
here.

First, we create a feature branch. It's good practice to link all feature
branches with an issue and to name feature branches descriptively. Our issue is
Issue #1, Create python hello world.

.. code-block:: shell

    % git flow feature start Issue-#1-create-python-hello-world

This should produce output like::

    Switched to a new branch 'feature/Issue-#1-create-python-hello-world'

    Summary of actions:
    - A new branch 'feature/Issue-#1-create-python-hello-world' was created, based on 'develop'
    - You are now on branch 'feature/Issue-#1-create-python-hello-world'

    Now, start committing on your feature. When done, use:

         git flow feature finish Issue-#1-create-python-hello-world

Doing the Hax
^^^^^^^^^^^^^

Next, we create the hello world file for our feature request and commit the
changes just as we would normally do. When committing, best practice is to
provide a 50 char or less commit summary on the first line, double space, then
provide a detailed summary of changes.

It's common to make multiple commits during the course of solving a problem.
Many of these frequently have innane commit messages like "enabling debugging",
"disabling debugging", "fix syntax", etc. Though helpful when working through a
problem, they're not very useful for tracking a projects history nor do they
capture the purpose of the original commit. Because of this, it's good practice
to clean up your commit messages. We can do this by rebasing.

Cleaning Up (Rebasing)
^^^^^^^^^^^^^^^^^^^^^^

Rebasing can be used to consolidate a collection of commits into a single
commit. This is done by 'squashing' all non-desired commits. To do so, we first
look at the log history and choose the most recent sha before our feature fork.
But first, we should rebase against the develop branch to catch any changes
since we forked.

.. code-block::

    % git rebase origin/develop
    % git log --pretty=format:"%C(yellow)%h%Cred%d %Creset%s%Cblue [%cn]" --decorate

I have the above git commmand saved to a gitalias

In my case, this outputs::

    85e2ec3 (HEAD, feature/Issue-#1-create-python-hello-world) syntax error [James Kyle]
    75e438b Issue #1  Add python hello world script. [James Kyle]
    9624aa4 (origin/develop, origin/HEAD, develop) Initial commit. [James Kyle]
    1385598 (origin/master, master) Initial commit [James Kyle]

This will, of course, be different for each case. But the sha we're interested
in is '9624aa4', the point where we branched from the develop branch.

.. code-block:: shell

    % git rebase -i 9624aa4

The interactive rebase (-i) editor will look something similar to::

    pick 75e438b Issue #1  Add python hello world script.
    pick 85e2ec3 syntax error
    pick a53dab1 Updated readme

    # Rebase 9624aa4..85e2ec3 onto 9624aa4
    #
    # Commands:
    #  p, pick = use commit
    #  r, reword = use commit, but edit the commit message
    #  e, edit = use commit, but stop for amending
    #  s, squash = use commit, but meld into previous commit
    #  f, fixup = like "squash", but discard this commit's log message
    #  x, exec = run command (the rest of the line) using shell
    #
    # These lines can be re-ordered; they are executed from top to bottom.
    #
    # If you remove a line here THAT COMMIT WILL BE LOST.
    #
    # However, if you remove everything, the rebase will be aborted.
    #
    # Note that empty commits are commented out

To squash, we just change 'pick' to 'squash' for all commits except one::

    pick 75e438b Issue #1  Add python hello world script.
    squash 85e2ec3 syntax error
    squash a53dab1 Updated readme

The rebase will consolidate all changes into a single page, then present the
developer with the option to modify the commit message. My squash screen looks
like::

    # This is a combination of 4 commits.
    # The first commit's message is:
    Issue #1  Add python hello world script.

    Addes a hello_world.py script that outputs "Hello, World!"

    # This is the 2nd commit message:

    syntax error

    # This is the 3rd commit message:

    Updated readme.

    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    # HEAD detached from 75e438b
    # You are currently editing a commit while rebasing branch 'feature/Issue-#1-create-python-hello-world' on '9624aa4'.#
    # Changes to be committed:
    #   (use "git reset HEAD^1 <file>..." to unstage)                               #
    #       modified:   README.rst
    #       new file:   hello_world.py
    #

My cleaned up message looks like::

    # This is a combination of 4 commits.
    # The first commit's message is:
    Issue #1  Add python hello world script.

    Addes a hello_world.py script that outputs "Hello, World!"

    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    # HEAD detached from 75e438b
    # You are currently editing a commit while rebasing branch 'feature/Issue-#1-create-python-hello-world' on '9624aa4'.#
    # Changes to be committed:
    #   (use "git reset HEAD^1 <file>..." to unstage)
    #
    #       modified:   README.rst
    #       new file:   hello_world.py
    #

Make sure to retain a reference to the issue in your comments. Most repository
applications will link this back to the issue itself. When you're done, save
and exit. If you look at your git-log output, it should have condense your
feature branch into a single, descriptive commit.

Publish & Submit the Pull Request
---------------------------------

All that's left is to submit a pull request. This can vary from application to
applicaiton. Two of the most popular are bitbucket_ and github_. With git-flow,
pull requests from feature branches are always made against the 'develop'
branch. Either way you'll need to push your changes so the merge master can pull
them. To do so with git-flow we 'publish' the branch.

.. code-block:: shell

    % git flow feature publish Issue-#1-create-python-hello-world

Git flow outputs the operations this triggers::

    Username for 'https://bitbucket.org': james@jameskyle.org
    Password for 'https://james@jameskyle.org@bitbucket.org':
    Counting objects: 4, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 375 bytes | 0 bytes/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    To https://bitbucket.org/jkyle/git-flow-tutorial.git
     * [new branch]      feature/Issue-#1-create-python-hello-world ->
       feature/Issue-#1-create-python-hello-world
       Already on 'feature/Issue-#1-create-python-hello-world'

     Summary of actions:
     - A new remote branch 'feature/Issue-#1-create-python-hello-world' was
       created
     - The local branch 'feature/Issue-#1-create-python-hello-world' was
       configured to track the remote branch
     - You are now on branch 'feature/Issue-#1-create-python-hello-world'

The rest is up to the merge master!

For Merge Masters
-----------------

If you _are_ the merge master git-flow makes it easy for you. After checking
out the remote feature branch

.. code-block:: shell

    % git flow feature finish Issue-#1-create-python-hello-world

Git flow outputs the actions this triggers::

    Switched to branch 'develop'
    Updating 9624aa4..d162b73
    Fast-forward
     hello_world.py | 3 +++
     1 file changed, 3 insertions(+)
     create mode 100644 hello_world.py
    Deleted branch feature/Issue-#1-create-python-hello-world (was d162b73).

    Summary of actions:
    - The feature branch 'feature/Issue-#1-create-python-hello-world' was merged into 'develop'
    - Feature branch 'feature/Issue-#1-create-python-hello-world' has been removed
    - You are now on branch 'develop'

.. _homepage: https://github.com/nvie/gitflow
.. _`nvie's blog post`: http://nvie.com/posts/a-successful-git-branching-model
.. _github: https://help.github.com/articles/using-pull-requests
.. _bitbucket: https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests

